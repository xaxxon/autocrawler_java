package autocrawler.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** list the files in framegrabs and streams folders */
public class MediaServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
    static final String IMAGE = "IMAGE";
    static final String VIDEO = "VIDEO";
	
	static String getTemplate() {
		StringBuffer str = new StringBuffer();
		str.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
		str.append("<!-- DO NOT MODIFY THIS FILE, THINGS WILL BREAK -->\n");
		str.append("<html><head><title>Autocrawler Media Files</title>\n" +
	                "<meta http-equiv=\"Pragma\" content=\"no-cache\">\n" +
	                "<meta http-equiv=\"Cache-Control\" Content=\"no-cache\">\n" +
	                "<meta http-equiv=\"Expires\" content=\"-1\">\n" +
					"<style type=\"text/css\">\n" +
	                "a { text-decoration: none; } \n" +
	                "body { padding-bottom: 10px; margin: 0px; padding: 0px} \n" +
	                "body, p, ol, ul, td, tr, td { font-family: verdana, arial, helvetica, sans-serif;}\n");
		str.append("."+IMAGE+" {background-color: #FF8533; padding-top: 3px; padding-bottom: 3px; padding-left: 15px; padding-right: 10px; border-top: 1px solid #ffffff; }\n");
		str.append("."+VIDEO+" {background-color: #C2EBFF; padding-top: 3px; padding-bottom: 3px; padding-left: 15px; padding-right: 10px; border-top: 1px solid #ffffff; }\n");
		str.append("</style>\n");   
     	str.append("</head><body>\n");
        str.append("<div style='padding-top: 5px; padding-bottom: 5px; padding-left: 15px; '><b>Autocrawler Media Files </b></div>\n");
        return str.toString();
	}
	
	public void doGet(final HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {

		if (!autocrawler.BanList.getRefrence().knownAddress(req.getRemoteAddr())) {
			autocrawler.Util.log("unknown address: sending to login: " + req.getRemoteAddr(), this);
			response.sendRedirect("/autocrawler");
			return;
		}
				
		String filterstr = null; // optional filter string 
		try { filterstr = req.getParameter("filter"); } catch (Exception e) {}
		
		StringBuffer page = new StringBuffer(getTemplate()); 
		File[] list = getSorted(filterstr);
		if ( list == null ) page.append ("<div style='padding-left: 15px'>No media yet</div>"); 
		else page.append(getTable(list));
		
		response.setContentType("html");
		java.io.PrintWriter out = response.getWriter();
        out.println(page + "</body></html>");
        out.close();	
	}
	
	File[] getSorted(String filter) {
	
		File[] streams = null;
		File[] frames = null;
		
		int frameslength = 0;		
		int streamslength = 0;

		if(filter == null){ // get all
			streams = new File(autocrawler.Settings.streamfolder).listFiles();
			frames = new File(autocrawler.Settings.framefolder).listFiles();
		} else { // get matches only 
			streams = new File(autocrawler.Settings.streamfolder).listFiles((File pathname) -> pathname.getName().contains(filter));
			frames = new File(autocrawler.Settings.framefolder).listFiles((File pathname) -> pathname.getName().contains(filter));	
		}

		if (frames != null) frameslength = frames.length;
		if (streams != null) streamslength = streams.length;
		if (frameslength + streamslength == 0) return null;
		
		int total = 0; // merge lists
		File[] files = new File[frameslength + streamslength]; 
		files = new File[frameslength + streamslength];
		for (int i = 0; i < streamslength; i++) files[total++] = streams[i];
		for (int j = 0; j < frameslength; j++)  files[total++] = frames[j];

		// sort by date
		java.util.Arrays.sort(files, new java.util.Comparator<File>() {
			public int compare(File f1, File f2) {
				return Long.compare(f2.lastModified(), f1.lastModified());
			}
		});
		
		return files;
	}
	
	StringBuffer getTable(File[] files) {
		
		Long totalbytes = 0L;
		StringBuffer str = new StringBuffer();
		str.append("\n<table> \n");
		for (int c = 0; c < files.length; c++) {
			
			if (files[c].length() == 0) {
				autocrawler.Util.log("deleted zero byte file: " + files[c].getAbsolutePath(), this);
				files[c].delete();
				continue;
			}
			
			totalbytes += files[c].length();

			if (files[c].getName().toLowerCase().endsWith(".jpg"))
				str.append("<tr><td><div class=\'" + IMAGE + "\'><a href=\"/autocrawler/framegrabs/" + files[c].getName()
						+ "\" target='_blank'>"
						+ files[c].getName() + "</a></div><td style=\"text-align: right;\">" + files[c].length() + " bytes" + "</tr>\n");

			else 
				str.append("<tr><td><div class=\'" + VIDEO + "\'><a href=\"/autocrawler/streams/" + files[c].getName()
						+ "\" target='_blank'>"
						+ files[c].getName() + "</a></div><td style=\"text-align: right;\">" + files[c].length() + " bytes" + "</tr>\n");
		}

		str.append("\n</table> \n");
		str.insert(0, "<div style='padding-top: 5px; padding-bottom: 5px; padding-left: 15px; '>Total: "+
				totalbytes/1000000L+"Mb in "+ files.length+" file(s)</div>\n");
		
		return str;
	}
}
	